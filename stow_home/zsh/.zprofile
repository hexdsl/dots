#!/bin/zsh

# exports
export PATH="${PATH}:/home/hexdsl/.bin"
export XDG_CACHE_HOME="$HOME/.cache"
export __GL_SHADER_DISK_CACHE=1
export __GL_SHADER_DISK_CACHE_SKIP_CLEANUP=1
export _JAVA_AWT_WM_NONREPARENTING=1
export XDG_CACHE_HOME="$HOME/.cache"
export SUDO_ASKPASS="$HOME/.bin/dmenupass"
export QT_QPA_PLATFORMTHEME="gtk2"
export FZF_DEFAULT_OPTS="--layout=reverse --height 40%"
export PROTON_FORCE_LARGE_ADDRESS_AWARE=1

# default applications
export EDITOR="nvim"
export TERMINAL="alacritty"
export TERMCMD="alacritty"
#export BROWSER="qutebrowser"
export READER="zathura"

# XDG shit
export XDG_DESKTOP_DIR="$HOME/Downloads"
export XDG_DOCUMENTS_DIR="$HOME/Documents"
export XDG_DOWNLOAD_DIR="$HOME/Downloads"
export XDG_MUSIC_DIR="$HOME/Music"
export XDG_PICTURES_DIR="$HOME/Pictures"
export XDG_VIDEOS_DIR="$HOME/Videos"
export XDG_CACHE_HOME="$HOME/.cache"

# calibre
export CALIBRE_USE_DARK_PALETTE=1

# fff
export FFF_FAV1=~
export FFF_FAV2=~/Downloads
export FFF_FAV3=/mnt
export FFF_FAV4=~/Pictures
export FFF_FAV5=~/Documents
export FFF_FAV6=/mnt/rust1/obs
export FFF_FAV7=
export FFF_FAV8=
export FFF_FAV9=

# nnn
export NNN_BMS='h:~;p:~/Pictures;d:~/Downloads/;x:~/Insync/hexdsl@gmail.com/googledrive;o:~/dots;c:~/.config;m:/mnt/;b:bookmarks'
export NNN_PLUG='t:_|alacritty*;g:_|gimp $nnn*'
export NNN_USE_EDITOR=1
export NNN_OPENER=nnnopen
export NNN_COLORS='1267'

# STARTX init... like yeah
if systemctl -q is-active graphical.target && [[ ! $DISPLAY && $XDG_VTNR -eq 1 ]]; then
  exec startx -- vt1 &> /dev/null
fi
