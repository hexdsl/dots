#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

#Alias
alias ls='ls --color=auto'
alias cp='cp -v'
alias mv='mv -v'
alias df='df -h'
alias rank='sudo reflector --verbose -l 200 -p http --sort rate --save /etc/pacman.d/mirrorlist'
alias top='htop'
alias vim='nvim'
alias free='free -h'
alias whats='apropos'
alias sl='ls --color=auto'
alias pass='PASSWORD_STORE_ENABLE_EXTENSIONS=true pass'
alias myip="curl http://ipecho.net/plain; echo"
alias parrot='curl parrot.live'
alias si3='vim ~/.config/i3/config'
alias sqb='vim ~/.config/qutebrowser/config.py'
alias sbash='vim ~/.bashrc'
alias netusage='vnstat'
alias rtv='rtv --theme solarized-dark'
alias reddit='rtv --theme solarized-dark -s linux_gaming'
alias s='sudo systemctl'
alias ping='ping -c 3'
alias yt='ytcc'
alias ytupdate='ytcc -uld'
alias ytwatched='ytcc -um'
alias ytlist='ytcc -c'
alias augur='ssh 192.168.1.101 -l hexdsl'

# PS1 Setup


#Variables
export MAKEFLAGS="-j9 -l8"
export PATH="/usr/lib/ccache/bin/:/home/hexdsl/.bin/:$PATH"
BROWSER=/usr/bin/qutebrowser
EDITOR=/usr/bin/nvim

# Extract archive
function extract {
    if [ -z "$1" ]; then
        echo "Usage: extract <path/file_name>.<zip|rar|bz2|gz|tar|tbz2|tgz|Z|7z|xz|ex|tar.bz2|tar.gz|tar.xz>"
    else
        if [ -f $1 ] ; then
            case $1 in
                *.tar.bz2)   tar xvjf ./$1    ;;
                .tar.gz)    tar xvzf ./$1    ;;
                *.tar.xz)    tar xvJf ./$1    ;;
                *.lzma)      unlzma ./$1      ;;
                *.bz2)       bunzip2 ./$1     ;;
                *.rar)       unrar x -ad ./$1 ;;
                *.gz)        gunzip ./$1      ;;
                *.tar)       tar xvf ./$1     ;;
                *.tbz2)      tar xvjf ./$1    ;;
                *.tgz)       tar xvzf ./$1    ;;
                *.zip)       unzip ./$1       ;;
                *.Z)         uncompress ./$1  ;;
                *.7z)        7z x ./$1        ;;
                *.xz)        unxz ./$1        ;;
                *.exe)       cabextract ./$1  ;;
                *)           echo "extract: '$1' - unknown archive method" ;;
            esac
        else
            echo "$1 - file does not exist"
        fi
    fi
}

#RunCommand

[ -f ~/.fzf.bash ] && source ~/.fzf.bash
alias drivesync='ruby /opt/drivesync/drivesync.rb'
