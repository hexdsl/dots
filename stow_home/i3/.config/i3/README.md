## Hex's i3wm config explained....

There's a lot going on in here, I'm not going into deep detail as most of it is pretty straight forward but I'll talk about some of the stuff that may not be obvious.

I start at the top and work down, so please follow along with both files open... That could be fun.... Like a show and tell..

## Basics

First things first, lets remove window Headers because they are shit. I set a border of 3 pixels because its chunky enough to get spotted easy but not large enough to be a waste of space.

**Colours**
Just the variables for colours.

**Variables**
Set workspace names a plain numbers with the exception on workspace 10, that's where I run games so I made it a steam logo to spice things up!

I also set a browser variable and a terminal variable because sometimes when testing I may change these

**Worspace Locations**
I set the following

LEFT SCREEN: workspaces 1, 2, 3
CENTRE SCREEN: workspaces 4, 5, 6, 0
RIGHT SCREEN: workspaces 7, 8, 9

**Scratch pad**
I use a single scratch pad.

Send to pad: Mod+Shift+Minus
Toggle pad visibility: Mod+Minus (always centres)

## keys that Launch things!

Yes, I do remember and use *all* of these

* **mod+enter**: 			terminal
* **mod+escape**: 			Xfce4-Task manager (still my favoured GUI task manager)
* **mod+Shift+q**: 			Close Program
* **mod+r**: 				restart i3wm
* **mod+Shift+Notsign**: 	on my keyboard this is mod shift escape because its a 60%
* **mod+Shift+o**: 			Turn off screens
* **mod+d**: 				Rofi (as launcher)
* **mod+c**: 				Rofi (as list of running applications)
* **mod+q**: 				Qutebrowser (fire jailed)
* **mod+g**: 				Gimp
* **mod+u**: 				Project M (pulse audio)
* **mod+Shift+Numbersign**:	toggle VPN
* **mod+Numbersign**: 		show VPN status
* **mod+t**: 				file manager (Caja - opens to my obs project folder by default)
* **mod+Shift+t**: 			file manager (Ranger)
* **mod+o**: 				takes a window, floats it, makes it smaller, moves it to bottom left of the right hand monitor (i use this when i wanna watch a video while I'm working on something else)
* **mod+m**: 				m is for music, in my case Spotify
* **mod+s**: 				Steam (runtime)
* **mod+Shift+s**: 			Steam (Native)
* **mod+i**: 				Lutris
* **mod+Shift+i**: 			Itch client
* **mod+Shift+b**: 			OBS & Chatty
* **mod+b**: 				Just OBS
* **mod+z**: 				Deluge (its a torrent client)
* **mod+x**: 				Stream-Link-twitch-gui (I use it to watch twitch)
* **mod+p**: 				Select area, take screenshot
* **mod+Shift+p**: 			Take a screenshot of the middle monitor
* **mod+Backspace**: 		i3 easy focus, jump to an open window using hints
* **mod+Shift+Backspace**: 	i3 easy focus + KILL, use hints to kill an application
* **mod+w**: 				re-glitches my wallpaper
* **mod+Apostrophe**: 		Blue berry (Bluetooth manager)
* **mod+slash**: 			popup time and date (uses desktop notification)

## Things that Launch when i3 restarts (and at login)

* kill poly bar, launch poly bar (currently commented out, returned to I3
	blocks recentlt)
* Kill compton, launch compton
* set backdrop (glitch it too because that's how I like it)

## Things that happen only at login

* Loads unclutter application
* Loads Spotify (no notifications) on workspace 2
* Loads Redshift-gtk
* Loads Mate-Power-Manager
* Loads Nextcloud client

## Movement and control

* **Focus a window**: h, j, k, l
* **Move a window**: Shift + h, j, k, l (if its a floating windows it moves by 30 pixels)
* **Toggle Split direction**: Ctrl+V (I think of it as 'vertical toggle')
* **Toggle Fullscreen**: Mod+f
* **Toggle floating**: Mod+space
* **Send window to workspace**: Mod+Shift+Number
* **Focus Workspace**: Mod+Number

## Audio controls

This just uses Media keys
 my keyboard has the following set on an Fn layer

* Next Track: E
* Previous Track: Q
* Play/Pause Toggle: W
* Volume up: D
* Volume Down: S

## Make sure some applications are set to specific places or floated on open

This section is pretty self explanatory.. Just read it!

## Gaps

Smart gaps on
Smart Borders on

Inner gaps 20
outer gaps 0

## come theme stuff

This is where I use all of those colours That we defined at the top as well as setting ubuntu as my system wide font (I like it, I use Hack as my terminal font though)

I then call my RESTORE script that will open default applications on specific workspaces at launch.

The last line defines the file type as i3 (I have specific highlighting rules defined in nvim)


