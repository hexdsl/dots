" uoou based on monokai-phoenix

set background=dark
hi clear

if exists("syntax_on")
  syntax reset
endif

let colors_name="uoou"

" Below are the colours used for this colourscheme
" 
" 67	4
" 52
" 59
" NONE
" 08
" 00
" 88
" 208
" 13
" 09
" 197	1
" 22
" 95
" 59
" 102
" 112	2
" 141	5
" 103
" 00
" 23
" 22
" 130
" 197	1
" 52
" 15
" 81	6
" 81	6
" 236
" 15
" 15
" 185	3
" 221

" Popup Menu
" --------------------------
hi Pmenu ctermfg=15 ctermbg=23 cterm=NONE
hi PmenuSet ctermfg=6 ctermbg=NONE cterm=NONE
hi PmenuSBar ctermfg=6 ctermbg=59 cterm=NONE
hi PmenuSel ctermfg=6 ctermbg=59 cterm=NONE
hi PmenuThumb ctermfg=103 ctermbg=103 cterm=NONE

" Editor
" --------------------------
hi Normal ctermfg=15 ctermbg=NONE cterm=NONE
hi Conceal ctermfg=6 ctermbg=NONE cterm=NONE
hi NonText ctermfg=59 ctermbg=NONE cterm=NONE
hi StatusLine ctermfg=6 ctermbg=NONE cterm=NONE
hi StatusLineNC ctermfg=103 ctermbg=NONE cterm=NONE
hi Search ctermfg=08 ctermbg=221 cterm=NONE
hi VertSplit ctermfg=95 ctermbg=NONE cterm=NONE
hi LineNr ctermfg=102 ctermbg=NONE cterm=NONE
hi SignColumn ctermfg=NONE ctermbg=08 cterm=NONE

" Messages
" --------------------------
hi Question ctermfg=3 ctermbg=NONE cterm=NONE
hi ModeMsg ctermfg=3 ctermbg=NONE cterm=NONE
hi MoreMsg ctermfg=3 ctermbg=NONE cterm=NONE
hi ErrorMsg ctermfg=NONE ctermbg=197 cterm=NONE
hi WarningMsg ctermfg=1 ctermbg=NONE cterm=NONE

" Spelling
" --------------------------
hi SpellBad ctermfg=15 ctermbg=1 cterm=NONE
hi SpellLocal ctermfg=15 ctermbg=1 cterm=NONE

" Tabline
" --------------------------
hi TabLine ctermfg=102 ctermbg=NONE cterm=NONE
hi TabLineSel ctermfg=15 ctermbg=59 cterm=NONE
hi TabLineFill ctermfg=NONE ctermbg=NONE cterm=NONE

" Misc
" --------------------------
hi SpecialKey ctermfg=59 ctermbg=NONE cterm=NONE
hi Title ctermfg=3 ctermbg=NONE cterm=NONE
hi Directory ctermfg=6 ctermbg=NONE cterm=NONE

" Diff
" --------------------------
hi DiffAdd ctermfg=15 ctermbg=22 cterm=NONE
hi DiffDelete ctermfg=1 ctermbg=52 cterm=NONE
hi DiffChange ctermfg=3 ctermbg=52 cterm=NONE
hi DiffText ctermfg=95 ctermbg=NONE cterm=NONE

" Folding
" --------------------------
hi Folded ctermfg=103 ctermbg=08 cterm=NONE
hi FoldColumn ctermfg=NONE ctermbg=08 cterm=NONE

" Cursor colours
" --------------------------
hi ColorColumn ctermfg=NONE ctermbg=236 cterm=NONE
hi CursorColumn ctermfg=NONE ctermbg=236 cterm=NONE
hi CursorLine ctermfg=NONE ctermbg=236 cterm=NONE
hi CursorLineNr ctermfg=15 ctermbg=NONE cterm=NONE
hi Cursor ctermfg=NONE ctermbg=15 cterm=NONE
hi Visual ctermfg=NONE ctermbg=59 cterm=NONE
hi MatchParen ctermfg=NONE ctermbg=185 cterm=NONE

if has("nvim")

  " Neovim Terminal
  " --------------------------
  hi TermCursor ctermfg=NONE ctermbg=15 cterm=NONE
  hi TermCursorNC ctermfg=NONE ctermbg=103 cterm=NONE

endif

" General Highlighting
" --------------------------
hi Constant ctermfg=5 ctermbg=NONE cterm=NONE
hi Number ctermfg=5 ctermbg=NONE cterm=NONE
hi Float ctermfg=5 ctermbg=NONE cterm=NONE
hi Boolean ctermfg=5 ctermbg=NONE cterm=NONE
hi Character ctermfg=3 ctermbg=NONE cterm=NONE
hi String ctermfg=3 ctermbg=NONE cterm=NONE
hi Type ctermfg=6 ctermbg=NONE cterm=NONE
hi Structure ctermfg=1 ctermbg=NONE cterm=NONE
hi StorageClass ctermfg=1 ctermbg=NONE cterm=NONE
hi TypeDef ctermfg=1 ctermbg=NONE cterm=NONE
hi Identifier ctermfg=2 ctermbg=NONE cterm=NONE
hi Function ctermfg=2 ctermbg=NONE cterm=NONE
hi Statement ctermfg=1 ctermbg=NONE cterm=NONE
hi Operator ctermfg=1 ctermbg=NONE cterm=NONE
hi Label ctermfg=1 ctermbg=NONE cterm=NONE
hi Keyword ctermfg=6 ctermbg=NONE cterm=NONE
hi Preproc ctermfg=2 ctermbg=NONE cterm=NONE
hi Include ctermfg=6 ctermbg=NONE cterm=NONE
hi Define ctermfg=6 ctermbg=NONE cterm=NONE
hi Macro ctermfg=2 ctermbg=NONE cterm=NONE
hi PreCondit ctermfg=2 ctermbg=NONE cterm=NONE
hi Special ctermfg=6 ctermbg=NONE cterm=NONE
hi SpecialChar ctermfg=1 ctermbg=NONE cterm=NONE
hi Delimiter ctermfg=1 ctermbg=NONE cterm=NONE
hi Comment ctermfg=4 ctermbg=NONE cterm=italic
hi SpecialComment ctermfg=6 ctermbg=NONE cterm=NONE
hi Tag ctermfg=1 ctermbg=NONE cterm=NONE
hi Underlined ctermfg=2 ctermbg=NONE cterm=NONE
hi Ignore ctermfg=NONE ctermbg=NONE cterm=NONE
hi Todo ctermfg=15 ctermbg=NONE cterm=bold
hi Error ctermfg=15 ctermbg=88 cterm=NONE

function! s:cppMonokaiPhoenixFiletype()
  " vim-cpp-enhanced-highlight syntax
  " --------------------------
  hi cppSTLnamespace ctermfg=6 ctermbg=NONE cterm=NONE
  hi cppSTLtype ctermfg=6 ctermbg=NONE cterm=NONE
  hi cppModifier ctermfg=1 ctermbg=NONE cterm=NONE

endfunction

function! s:javascriptMonokaiPhoenixFiletype()
  " vim-javascript syntax
  " --------------------------
  hi jsStorageClass ctermfg=6 ctermbg=NONE cterm=NONE
  hi jsOperator ctermfg=1 ctermbg=NONE cterm=NONE
  hi jsBooleanTrue ctermfg=5 ctermbg=NONE cterm=NONE
  hi jsBooleanFalse ctermfg=5 ctermbg=NONE cterm=NONE
  hi jsModules ctermfg=1 ctermbg=NONE cterm=NONE
  hi jsModuleWords ctermfg=1 ctermbg=NONE cterm=NONE
  hi jsOf ctermfg=1 ctermbg=NONE cterm=NONE
  hi jsArgsObj ctermfg=6 ctermbg=NONE cterm=NONE
  hi jsImportContainer ctermfg=1 ctermbg=NONE cterm=NONE
  hi jsExportContainer ctermfg=1 ctermbg=NONE cterm=NONE
  hi jsCommentTodo ctermfg=103 ctermbg=NONE cterm=NONE
  hi jsLineComment ctermfg=4 ctermbg=NONE cterm=italic
  hi jsEnvComment ctermfg=4 ctermbg=NONE cterm=italic
  hi jsCvsTag ctermfg=4 ctermbg=NONE cterm=italic
  hi jsComment ctermfg=4 ctermbg=NONE cterm=italic
  hi jsBlockComment ctermfg=4 ctermbg=NONE cterm=italic
  hi jsDocTags ctermfg=103 ctermbg=NONE cterm=NONE
  hi jsDocType ctermfg=103 ctermbg=NONE cterm=NONE
  hi jsDocTypeNoParam ctermfg=103 ctermbg=NONE cterm=NONE
  hi jsDocParam ctermfg=103 ctermbg=NONE cterm=NONE
  hi jsDocSeeTag ctermfg=103 ctermbg=NONE cterm=NONE
  hi jsTemplateVar ctermfg=3 ctermbg=NONE cterm=NONE
  hi jsStringD ctermfg=3 ctermbg=NONE cterm=NONE
  hi jsStringS ctermfg=3 ctermbg=NONE cterm=NONE
  hi jsTemplateString ctermfg=3 ctermbg=NONE cterm=NONE
  hi jsTemplateBraces ctermfg=2 ctermbg=NONE cterm=NONE
  hi jsTaggedTemplate ctermfg=3 ctermbg=NONE cterm=NONE
  hi jsRegexpCharClass ctermfg=3 ctermbg=NONE cterm=NONE
  hi jsRegexpBoundary ctermfg=3 ctermbg=NONE cterm=NONE
  hi jsRegexpBackRef ctermfg=3 ctermbg=NONE cterm=NONE
  hi jsRegexpQuantifier ctermfg=3 ctermbg=NONE cterm=NONE
  hi jsRegexpOr ctermfg=3 ctermbg=NONE cterm=NONE
  hi jsRegexpMod ctermfg=3 ctermbg=NONE cterm=NONE
  hi jsRegexpSpecial ctermfg=3 ctermbg=NONE cterm=NONE
  hi jsRegexpGroup ctermfg=3 ctermbg=NONE cterm=NONE
  hi jsRegexpString ctermfg=3 ctermbg=NONE cterm=NONE
  hi jsNumber ctermfg=5 ctermbg=NONE cterm=NONE
  hi jsFloat ctermfg=5 ctermbg=NONE cterm=NONE
  hi jsObjectKey ctermfg=3 ctermbg=NONE cterm=NONE
  hi jsFunctionKey ctermfg=2 ctermbg=NONE cterm=NONE
  hi jsDecorator ctermfg=2 ctermbg=NONE cterm=NONE
  hi jsDecoratorFunction ctermfg=2 ctermbg=NONE cterm=NONE
  hi jsStatement ctermfg=1 ctermbg=NONE cterm=NONE
  hi jsConditional ctermfg=1 ctermbg=NONE cterm=NONE
  hi jsRepeat ctermfg=1 ctermbg=NONE cterm=NONE
  hi jsLabel ctermfg=1 ctermbg=NONE cterm=NONE
  hi jsKeyword ctermfg=1 ctermbg=NONE cterm=NONE
  hi jsClass ctermfg=6 ctermbg=NONE cterm=NONE
  hi jsAsyncKeyword ctermfg=6 ctermbg=NONE cterm=NONE
  hi jsGlobalObjects ctermfg=6 ctermbg=NONE cterm=NONE
  hi jsGlobalNodeObjects ctermfg=6 ctermbg=NONE cterm=italic
  hi jsThis ctermfg=6 ctermbg=NONE cterm=italic
  hi jsExceptions ctermfg=1 ctermbg=NONE cterm=NONE
  hi jsBuiltins ctermfg=6 ctermbg=NONE cterm=NONE
  hi jsFutureKeys ctermfg=6 ctermbg=NONE cterm=NONE
  hi jsHtmlEvents ctermfg=6 ctermbg=NONE cterm=NONE
  hi jsTernaryIfOperator ctermfg=1 ctermbg=NONE cterm=NONE
  hi jsGenerator ctermfg=1 ctermbg=NONE cterm=NONE
  hi jsFuncName ctermfg=2 ctermbg=NONE cterm=NONE
  hi jsFuncArgs ctermfg=208 ctermbg=NONE cterm=NONE
  hi jsFuncArgRest ctermfg=1 ctermbg=NONE cterm=NONE
  hi jsArrowFunction ctermfg=6 ctermbg=NONE cterm=NONE
  hi jsFuncCall ctermfg=2 ctermbg=NONE cterm=NONE

  " Custom syntax
  " --------------------------
  hi javaScriptMethodCallWithoutArgs ctermfg=2 ctermbg=NONE cterm=NONE
  hi javaScriptMethodCallWithArgs ctermfg=2 ctermbg=NONE cterm=NONE
  hi javaScriptStaticFunctionWithArgs ctermfg=2 ctermbg=NONE cterm=NONE
  hi javaScriptStaticFunctionWithoutArgs ctermfg=2 ctermbg=NONE cterm=NONE
  hi javaScriptFunctionCallWithArgs ctermfg=2 ctermbg=NONE cterm=NONE
  hi javaScriptFunctionCallWithoutArgs ctermfg=2 ctermbg=NONE cterm=NONE
  hi javaScriptArrowFunction ctermfg=6 ctermbg=NONE cterm=NONE

endfunction

function! s:jsonMonokaiPhoenixFiletype()
  " JSON Syntax
  " --------------------------
  hi jsonNull ctermfg=5 ctermbg=NONE cterm=NONE
  hi jsonKeyword ctermfg=15 ctermbg=NONE cterm=NONE
  hi jsonPadding ctermfg=15 ctermbg=NONE cterm=NONE
  hi jsonBraces ctermfg=15 ctermbg=NONE cterm=NONE

endfunction

function! s:pursMonokaiPhoenixFiletype()
  " Purescript Syntax
  " --------------------------
  hi purescriptImportKeyword ctermfg=1 ctermbg=NONE cterm=NONE
  hi purescriptConstructor ctermfg=5 ctermbg=NONE cterm=NONE
  hi purescriptConstructorDecl ctermfg=5 ctermbg=NONE cterm=NONE
  hi purescriptTypeAlias ctermfg=5 ctermbg=NONE cterm=NONE
  hi purescriptModuleKeyword ctermfg=1 ctermbg=NONE cterm=NONE
  hi purescriptWhere ctermfg=1 ctermbg=NONE cterm=NONE
  hi purescriptData ctermfg=1 ctermbg=NONE cterm=NONE
  hi purescriptTypeVar ctermfg=15 ctermbg=NONE cterm=NONE
  hi purescriptDelimiter ctermfg=15 ctermbg=NONE cterm=NONE

endfunction

augroup MONOKAI_PHOENIX_FILETYPE_LOADER
  au!
  au Filetype purs call <SID>pursMonokaiPhoenixFiletype()
  au Filetype cpp call <SID>cppMonokaiPhoenixFiletype()
  au Filetype json call <SID>jsonMonokaiPhoenixFiletype()
  au Filetype javascript call <SID>javascriptMonokaiPhoenixFiletype()
augroup END

