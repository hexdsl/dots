" HexDSL NeoVim Config. LAST MAJOR UPDATE: 16/11/2020

" Plugin manager (Install if not found)
if empty(glob('~/.config/nvim/autoload/plug.vim'))
	silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
				\ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	autocmd VimEnter * PlugInstall 
endif

"Plug in manager, managing them plugs.
call plug#begin('~/.config/nvim/plugged')
	"Plug 'chaimleib/vim-renpy' "Language complete.
	Plug 'unblevable/quick-scope' "press f to see jump points.
	Plug 'mcchrish/nnn.vim' "File Manager.
	Plug 'itchyny/lightline.vim' "Bar at the bottom.
	Plug 'junegunn/fzf', { 'do': { -> fzf#install() } } "Find.
	Plug 'junegunn/fzf.vim' "Find.
	Plug 'mbbill/undotree' "UnDo with F5
	Plug 'luochen1990/rainbow' "Rainbow Parentheses and shit.
	Plug 'Ron89/thesaurus_query.vim' "Thesaurus (Offline by default.)
	Plug 'itchyny/vim-cursorword' "underline the word under the cursor.
	Plug 'jiangmiao/auto-pairs' "Pairsa
	"Plug 'prabirshrestha/vim-lsp'
	"Plug 'mattn/vim-lsp-settings'
	Plug 'neoclide/coc.nvim', {'branch': 'master', 'do': 'yarn install --frozen-lockfile'}
	call plug#end()

" tabbery
function! Smart_TabComplete()
  let line = getline('.')                         " current line

  let substr = strpart(line, -1, col('.')+1)      " from the start of the current
                                                  " line to one character right
                                                  " of the cursor
  let substr = matchstr(substr, "[^ \t]*$")       " word till cursor
  if (strlen(substr)==0)                          " nothing to match on empty string
    return "\<tab>"
  endif
  let has_period = match(substr, '\.') != -1      " position of period, if any
  let has_slash = match(substr, '\/') != -1       " position of slash, if any
  if (!has_period && !has_slash)
    return "\<C-X>\<C-P>"                         " existing text matching
  elseif ( has_slash )
    return "\<C-X>\<C-F>"                         " file matching
  else
    return "\<C-X>\<C-O>"                         " plugin matching
  endif
endfunction

inoremap <tab> <c-r>=Smart_TabComplete()<CR>
" --------------

augroup qs_colors
  autocmd!
  autocmd ColorScheme * highlight QuickScopePrimary guifg='#afff5f' gui=underline ctermfg=155 cterm=underline
  autocmd ColorScheme * highlight QuickScopeSecondary guifg='#5fffff' gui=underline ctermfg=81 cterm=underline
augroup END

nnoremap <F5> :UndotreeToggle<CR>

let g:qs_highlight_on_keys = ['f', 'F', 't', 'T']


set shortmess=I " Read :help shortmess for everything else.
set background=dark
set backspace=indent,eol,start
set clipboard=unnamedplus
set cursorline
set grepprg=grep\ -nH\ $*
set hidden
set history=10
set hlsearch
set incsearch
set laststatus=2
set lazyredraw
set linebreak
set list
set incsearch
set listchars=tab:__ "I like my tabs, visualised
set modelines=1
set mouse=a
set nobackup
set noerrorbells visualbell t_vb=
set noshowmode
set nowritebackup
set number
set relativenumber
set ruler
set showmatch
set spell spelllang=en_gb
set spellsuggest=best,10
set splitbelow
set splitright
set statusline =\ D:%{getcwd()}  " Working directory
set statusline+=\ F:%f           " Current file
set statusline+=\ L:%l/%L        " Current line vs lines number
set statusline+=\ R:%r           " File's permissions
set statusline+=\ S:%m           " File's modification state
set statusline+=\ T:%y           " File's language type
set textwidth=0
set wrap
set wrapmargin=0
set wrapscan
set splitbelow
set splitright
set nocindent
set nosmartindent
set noautoindent
let mapleader=" "
filetype indent off
filetype plugin indent off

"set viminfo^=%

" fix my lazy Shift finger
command W w
command Q q
command X x

" Don't judge me
nnoremap j gj
nnoremap k gk

"got sick of typing this
nnoremap S :%s//g<Left><Left>

" fix priv' escalation
cmap w!! w !sudo tee > /dev/null %

" Registers
map <A-r> :reg<CR>
command! WipeReg for i in range(34,122) | silent! call setreg(nr2char(i), []) | endfor

"Type the date-header, the two ways i need it done.
map <A-d> o DATE: <ESC>:r!date "+\%x"<ESC>kJA BY: HexDSL@posteo.net<ESC>kJA
map <C-d> ggO---<ESC>odate:<ESC>:r!date "+\%F"<ESC>kJo---<ESC>o

" World counting 
map <C-w> :!wc -w %<CR>

"file manager
let g:nnn#layout = { 'window': { 'width': 0.9, 'height': 0.6, 'highlight': 'Debug' } }

" Alt P to update plugins (Obviously) 
map <F1> :PlugInstall<CR>

"that shitty Auto-make pdf script (Must re-write)
map <leader>c :!setsid auto_vim_compile % &<CR><CR>
map <leader>o :!pandoc % -t beamer -o %:r.pdf &<CR><CR>
map <leader>p :!zathura %:r.pdf &<CR><CR>
map <A-f> gg=G
autocmd VimLeave * !pkill -9 "entr"

"too many key maps, this one reminds me
nmap <leader><tab> <plug>(fzf-maps-n)
omap <leader><tab> <plug>(fzf-maps-o)
xmap <leader><tab> <plug>(fzf-maps-x)

"kill current search
nnoremap <leader><space> :nohlsearch<CR>
"set incsearch
"set nohlsearch

"Files manager on C-f and search on C-p
nnoremap <c-f> ::NnnPicker %:p:h<CR>
nnoremap <C-p> :Files %:h<CR>

" Buffer Stuff (All on CTRL) 
map <C-n> :Buffers<CR>
map <C-b> :Buffers<CR>
map <C-Up> :bnext<CR>
map <C-Down> :bprevious<CR>

" Spelling checking and local thesaurus.
let g:tq_mthesaur_file="~/.config/nvim/mthesaur.txt"
let g:tq_enabled_backends=["mthesaur_txt","datamuse_com"]
let g:tq_online_backends_timeout = 0.4
map <A-t> :ThesaurusQueryReplaceCurrentWord<CR>
map <leader>s :setlocal spell!<CR>
map <C-Left> [s
map <C-Right> ]s
map <C-g> z=
map <A-g> ea<C-X>s
set spellfile=/home/hexdsl/Insync/hexdsl@gmail.com/googledrive/Backups/vim/en.utf-8.add

" Tab stuff (All on ALT) 
nnoremap <A-Up> :tabprevious<CR>
nnoremap <A-Down> :tabnext<CR>
nnoremap <A-n> :tabnew<CR>
nnoremap <C-c> :bd<CR>
nnoremap <A-c> :tabclose<CR>
inoremap <A-n> <Esc>:tabnew<CR>

" Splitting (C-HJKL to nav splits)
nnoremap <A-s> :vsp<CR>
nnoremap <C-s> :split<CR>
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" Splitting (A-HJKL to resizee)
noremap <A-h> :vertical resize -5<CR>
noremap <A-l> :vertical resize +5<CR>
noremap <A-j> :resize -5<CR>
noremap <A-k> :resize +5<CR>

" Dear YouTube, its an edited Inkpot. Stop asking me, plz!
colorscheme inkpot
":hi CursorLine   cterm=NONE ctermfg=white ctermbg=black
":hi CursorColumn cterm=NONE ctermfg=white ctermbg=black
:highlight CursorLine gui=underline cterm=underline


" autocmd stuff that I'm not sure I care about
autocmd FileType * set tabstop=4|set shiftwidth=4|set noexpandtab
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
autocmd FileType,BufRead *.rpy,*.rpyc set tabstop=4|set shiftwidth=4|set expandtab
autocmd BufRead,BufNewFile /tmp/calcurse*,~/.calcurse/notes/* set filetype=markdown
autocmd BufRead,BufNewFile /tmp/calcurse*,~/.calcurse/notes/* set filetype=markdown
autocmd BufRead,BufNewFile *.ms,*.me,*.mom set filetype=groff
autocmd BufRead,BufNewFile *.md set filetype=markdown
autocmd BufRead,BufNewFile *.gmi set filetype=markdown 
autocmd BufRead,BufNewFile *.tex set filetype=tex
autocmd BufRead,BufNewFile *.h,*.c set filetype=c 
autocmd FileType markdown let b:coc_suggest_disable = 1
autocmd FileType mail let b:coc_suggest_disable = 1

" put this at bottom to make sure spelling colours take
hi clear SpellBad
hi clear SpellCap
hi clear SpellLocal
hi clear SpellRare
hi SpellBad cterm=underline ctermfg=red
hi SpellCap cterm=underline ctermfg=blue 
hi SpellLocal cterm=underline ctermfg=green 
hi SpellRare cterm=underline,bold ctermfg=red 
