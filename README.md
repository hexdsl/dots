# Hexdsl's DOT files.

My Dotfiles use STOW.

	cd ~
	git clone git@gitlab.com:hexdsl/dots.git
	cd dots
	cd stow_home
	stow -t ~ [application name]

Or Want this with minimum stress then use:

	curl -LO hexdsl.co.uk/hextool
	Then use './hextool'

But don't! That would be stupid unless you are HexDSL! It's made for HexDSL and he doesn't need to be told about it  because he is me, therefore, not you!

![SCREEN SHOT](sshot.png)

I use DWM as my window manager. I dont like gaps or bars or transparency. 'nnn' is my file manager (With a little Thunar for GUI drag & droppings) and NeoVim as my editor. I also use an ErgoDox keyboard so the shortcuts in my DWM config will be nonsensical to you. Don't try and use my dot files wholesale, you wont have a good time. If you take a look at how I have things set up and want to take bits and bobs then please do, after all, that is how I created them.

Most of this stuff was stolen form [uoou](https://gitlab.com/uoou) and various posts on reddit. I did very little original.

**DWM:Torrents (currently uses transmission-remote/cli)**

* HYPER+t	=	Transmission menu via rofi

**DWM: Media Keys**
These make sense on *my* keyboard as I did some remapping (dont ask, it makes
sense, I promise)

* MEH+q		=	Previous track
* MEH+w		=	Toggle Play/Pause
* MEH+e		=	Next Track

**DWM: Launchers:**

* HYPER+b	=	OBS
* HYPER+c	=	colour code picker (gcolor)
* HYPER+d	=	Launcher (currently rofi)
* HYPER+e	=	Email (neomutt)
* HYPER+ENTR=	Time and info popup (works best with Dunst)
* HYPER+ESC	=	HTOP (Task manager)
* HYPER+g	=	GIMP
* HYPER+i	=	Mixer (currently pulsemixer)
* HYPER+k	=	kdenlive
* HYPER+m	=	music menu (Rofi Powered)
* HYPER+n	=	Newboat
* HYPER+r	=	GUI file manager (thunnar)
* HYPER+s	=	Steam
* HYPER+v	=	Clipboard Menu (Clipmenu)
* HYPER+w	=	cli browser (Currently Links)
* HYPER+y	=	Youtube Downloading menu (Dmenu)
* MEH+b		=	OBS AND Chatty
* MEH+c		=	IRC (WeeChat)
* MEH+ENTR	=	this odd piping script that I found on git (Work in progress)
* MEH+g		=	this odd piping script that I found on git (Work in progress)
* MEH+r		=	CLI file manager (Ranger)
* MEH+v		=	clipboard manager (Clipmenu)
* MOD+BkSpc	=	Sleep PC
* MOD+b		=	Toggle DWM BAR (Off by default)
* MOD+.		=	Centre application
* MOD+CTRL+H/L = Change split width
* MOD+d		=	Launcher (currently Rofi)
* MOD+ENTR	=	Change master item
* MOD+h/l	=	Focus left right screen
* MOD+j/k	=	Focus stack up down
* MOD+n		=	Status of VPN connection
* MOD+num	=	switch to tag (Workspace)
* MOD+o/i	=	Change number of stacks
* MOD+q		=	close application
* MOD+sh+Bk =	TURN OFF PC
* MOD+Sh+ck	=	Toggle Compositor (Compton-git)
* MOD+Sh+d	=	Rofi emoji list
* MOD+sh+ENT=	Terminal (alacritty)
* MOD+Sh+h/l=	move selected left right screen
* MOD+Sh+num=	Send selected to tag (Workspace)
* MOD+Sh+n	=	VPN Connection menu (Rofi)
* MOD+Sh+q	=	close application
* MOD+Sh+r	=	RESTART DWM
* MOD+U		=	Screen Resolution menu (Rofi)
* MOD+x		=	xkill

There are also some applications set to autostart at login/boot (Most are
assigned specific tags/monitors)

* firefox
* Mutt
* Nextcloud client
* steam
* terminal
* IRC Chat

Yes... This was a lot to take in, I know.
